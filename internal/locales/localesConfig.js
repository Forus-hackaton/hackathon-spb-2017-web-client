module.exports = {
	path: "src/locales",
	fileExtension: ".json",
	fileExclude: /_old$/,
	importDir: "tmp",
	importFileName: "i18n-import.xlsx",
	exportDir: "tmp",
	exportFileName: "i18n-export.xlsx"
};