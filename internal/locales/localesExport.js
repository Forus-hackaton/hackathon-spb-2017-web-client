const fs = require("fs");
const path = require("path");
const XLSX = require("xlsx");

const config = require("./localesConfig");
const localesPath = path.resolve(config.path);
const extension = config.fileExtension;
const excludeFileName = config.fileExclude;
const exportDir = path.resolve(config.exportDir);
const exportFileName = config.exportFileName;

if (!fs.existsSync(localesPath) || !fs.lstatSync(localesPath).isDirectory()) {
	throw new Error(`No directory ${localesPath}`);
}

const languages = [];
const namespaces = [];
const stucture = {
	namespaces: []
};


fs.readdirSync(localesPath).map(lang => {
	let langPath = path.join(localesPath, lang);
	if (!fs.lstatSync(langPath).isDirectory())
		return;

	languages.push(lang);
	fs.readdirSync(langPath).map(np => {
		let ext = path.extname(np);
		let name = path.basename(np, ext);
		if (ext !== extension || excludeFileName.test(name))
			return;

		if (namespaces.indexOf(name) === -1)
			namespaces.push(name);
	});
});


namespaces.forEach(name => {
	let namespace = {
		name,
		translations: {}
	};

	languages.forEach(lang => {
		let translationPath = path.join(localesPath, lang, `${name}${extension}`);
		if (!fs.existsSync(translationPath))
			return;

		let src = fs.readFileSync(translationPath, "utf-8");

		let keys = JSON.parse(src);
		for (let key in keys) {
			if (key in namespace.translations) {
				namespace.translations[key][lang] = keys[key];
			} else {
				namespace.translations[key] = {
					key,
					[lang]: keys[key]
				}
			}
		}
	});

	// Map for xlsx
	namespace["data"] = [];
	for (let key in namespace.translations) {
		let translation = namespace.translations[key];
		let row = [];
		row.push(translation.key);
		languages.forEach(lang => row.push(translation[lang] || null));
		namespace["data"].push(row);
	}
	stucture.namespaces.push(namespace);
});


let headerCellStyle = {
	hpx: 50
};
let keyColStyle = {
	wpx: 200,
	hpx: 30
};
let langColStyle = {
	wpx: 200,
	hpx: 30,
};

let styles = {
	"!cols": [keyColStyle],
	"!rows": [headerCellStyle]
};

let header = [];
header.push("KEY");
languages.forEach(lang => {
	header.push(lang.toUpperCase())
	styles["!cols"].push(langColStyle);
});


class Workbook {
	constructor() {
		this.SheetNames = [];
		this.Sheets = {};
	}

	addWS(name, data) {
		this.SheetNames.push(name);
		this.Sheets[name] = data;
	}
}

const wb = new Workbook();
stucture.namespaces.forEach(namespace => {
	namespace.data.unshift(header);
	let ws = XLSX.utils.aoa_to_sheet(namespace.data);
	for (let prop in styles)
		ws[prop] = styles[prop];

	wb.addWS(namespace.name, ws);
});

if (!fs.existsSync(exportDir))
	fs.mkdirSync(exportDir);

let filePath = path.join(exportDir, exportFileName);
XLSX.writeFile(wb, filePath, { bookType: "xlsx", bookSST: true });

