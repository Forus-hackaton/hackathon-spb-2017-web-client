const fs = require("fs");
const path = require("path");
const XLSX = require("xlsx");

const config = require("./localesConfig");
const localesPath = config.path;
const extension = config.fileExtension;
const excludeFileName = config.fileExclude;
const importFile = path.join(config.importDir, config.importFileName);

if (!fs.existsSync(importFile)) {
	throw new Error(`No file ${importFile}`);
}

let wb = XLSX.readFile(importFile);

let namespaces = wb.SheetNames;
let languages = {};

const addTranslation = (lang, np, key, value) => {
	if (!(lang in languages))
		languages[lang] = {};
	if (!(np in languages[lang]))
		languages[lang][np] = {};

	languages[lang][np][key] = value;
};

namespaces.forEach(np => {
	let translations = XLSX.utils.sheet_to_json(wb.Sheets[np]);

	translations.forEach(tr => {
		let key = null;
		let trLangs = {};
		for (let col in tr) {
			if (col.toLowerCase() === "key") {
				key = tr[col];
			} else {
				trLangs[col.toLowerCase()] = tr[col];
			}
		}

		for (let lang in trLangs)
			addTranslation(lang, np, key, trLangs[lang]);
	});
});

if (!fs.existsSync(localesPath))
	fs.mkdirSync(localesPath);

for (let lang in languages) {
	let langPath = path.join(localesPath, lang);
	if (!fs.existsSync(langPath)) {
		fs.mkdirSync(langPath);
	}

	for (let np in languages[lang]) {
		let translations = languages[lang][np];
		let filePath = path.join(langPath, `${np}.json`);

		if (fs.existsSync(filePath)) {
			let oldTranslations = JSON.parse(fs.readFileSync(filePath, "utf-8"));
			translations = Object.assign({}, oldTranslations, translations);
		}

		let fileBody = JSON.stringify(translations, null, 2);
		fs.writeFileSync(filePath, fileBody);
	}
}