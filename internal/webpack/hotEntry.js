const config = require("../config");

module.exports = (...src) => {
	return [
		"webpack-dev-server/client?http://localhost:" + config.server.port,
		"webpack/hot/dev-server",
		...src
	]
}