const webpack = require("webpack");
const WebpackDevServer = require("webpack-dev-server");
const path = require("path");

const config = require("./config");
const webpackConfig = require("./webpack/webpack.dev");

const server = new WebpackDevServer(webpack(webpackConfig), {
	contentBase: [path.resolve(__dirname, "../src")],
	hot: true,
	quiet: false,
	stats: { colors: true },
	historyApiFallback: true,
	proxy: {
		"/api": {
			target: "https://api.staging.giga-watt.com",
			secure: true,
			changeOrigin: true
		}
	}
});

server.listen(config.server.port, "localhost", (error) => {
	if (error) 
		console.error(error);

	console.info("Listening at localhost:" + config.server.port);
});
