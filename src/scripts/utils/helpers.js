export default function makeActionCreator(type, ...argNames) {
	return function (...args) {
		let action = { type };
		argNames.forEach((arg, index) => {
			action[argNames[index]] = args[index];
		});
		return action;
	};
}

export const createReducer = (initialState, actionHandlers) => {
	return (state = initialState, action) => {
		if (action.type in actionHandlers)
			return actionHandlers[action.type](state, action);
		else
			return state;
	};
};

export const generateUUID = () => {
	let d = Date.now();
	let uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, c => {
		let r = (d + Math.random() * 16) % 16 | 0;
		d = Math.floor(d / 16);
		return (c == "x" ? r : (r & 0x3 | 0x8)).toString(16);
	});
	return uuid;
};