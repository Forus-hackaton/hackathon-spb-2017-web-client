import { combineReducers } from "redux";

import AuthReducer from "auth/authReducer";
import CommonReducer from "common/commonReducer";

const combineReducer = combineReducers({
	auth: AuthReducer,
	common: CommonReducer
});

const reducer = (state, action) => combineReducer(state, action);

export default reducer;
