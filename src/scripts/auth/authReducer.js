import storage from "local-storage-fallback";

import * as Config from "config";
import { createReducer } from "utils/helpers";
import * as ActionTypes from "./authActionTypes";

const initialState = {
	isAuthorized: false
};
const accessTokenStorageKey = Config.accessTokenStorageKey;
const handlers = {};

handlers[ActionTypes.RESTORE_AUTH] = (state, action) => {
	const accessToken = storage.getItem(accessTokenStorageKey);
	return Object.assign({}, state, {
		isAuthorized: accessToken !== null ? true : false
	});
};

handlers[ActionTypes.SET_AUTH_TOKEN] = (state, action) => {
	const accessToken = action.accessToken;
	if(accessToken === null)
		return;

	storage.setItem(accessTokenStorageKey, accessToken);
	return Object.assign({}, state, {
		isAuthorized: true
	});
};

handlers[ActionTypes.SIGNOUT] = (state, action) => {
	storage.removeItem(accessTokenStorageKey);
	return Object.assign({}, state, {
		isAuthorized: false
	});
};

handlers[ActionTypes.RESET] = () => initialState;

export default createReducer(initialState, handlers);