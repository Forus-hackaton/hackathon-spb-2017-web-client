import * as ActionTypes from "./authActionTypes";

export const restoreAuth = () => ({
	type: ActionTypes.RESTORE_AUTH
});

export const setAuthToken = accessToken => ({
	type: ActionTypes.SET_AUTH_TOKEN,
	accessToken
});

export const signout = () => ({
	type: ActionTypes.SIGNOUT
});

export const resetAuthState = () => ({
	type: ActionTypes.RESET
});