import { createReducer } from "utils/helpers";

import * as ActionTypes from "./commonActionTypes";

const initialState = {
	currentLanguage: null
};
const handlers = {};

handlers[ActionTypes.SET_CURRENT_LANGUAGE] = (state, action) => {
	return {
		...state,
		currentLanguage: action.lang
	};
};

export default createReducer(initialState, handlers);