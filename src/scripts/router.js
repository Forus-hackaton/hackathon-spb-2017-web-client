import React from "react";
import { Router, Route, IndexRoute } from "react-router";

import Root from "routes/Root/Root";
import ErrorPage from "routes/ErrorPage/ErrorPage";
import AuthGate from "routes/AuthGate/AuthGate";
import PublicHome from "routes/public/PublicHome/PublicHome";

const CustomRouter = (props) => {
	return (
		<Router history={ props.history }>
			<Route component={ Root }>
				<Route path={"/"} component={ PublicHome }/>
			</Route>
			<Route path={"/authorization-gate"} component={ AuthGate }/>
			<Route path={"*"} component={ ErrorPage }/>
			<Route path={"/404"} component={ ErrorPage }/>
		</Router>
	);
};

export default CustomRouter;