import { Component } from "react";
import {connect} from "react-redux";

import "./Root.less";

const mapStateToProps = state => ({});
const mapDispatchToProps = {};

class Root extends Component {
	render = () => this.props.children;
}

export default connect(mapStateToProps, mapDispatchToProps)(Root);