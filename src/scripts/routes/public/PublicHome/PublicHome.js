import React, { Component } from "react";
import {connect} from "react-redux";

import styles from "./PublicHome.less";

const mapStateToProps = state => ({});
const mapDispatchToProps = {};

class PublicHome extends Component {
	render = () => {
		return (
			<div className={styles.root}>
				<div className={styles.banner}>
					<div className={styles.bannerContainer}>
						<div className={styles.bannerSearch}>
						
						</div>
					</div>
				</div>
				
				<div className={styles.footer}>
					<div className={styles.footerContainer}>
						<div className={styles.copyright}>
							© HealthChain
						</div>
					</div>
				</div>
			</div>
		);
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(PublicHome);