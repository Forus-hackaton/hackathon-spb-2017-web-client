import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import qs from "qs";

import styles from "./AuthGate.less";

const mapStateToProps = state => ({});
const mapDispatchToProps = {};

const USER_IMAGE = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPlimlADf1s3DQV-Wworyy6DdYFuyfgpH5GNBpkC4WbQyhvUU0sg";

const permissions = [{
	id: 1,
	title: "Общая информация",
	description: "Учреждение получит доступ к общим данным: ФИО, дата рождения и фотография"
}, {
	id: 2,
	title: "История заболеваний",
	description: "Список заболеваний и стадий развития болезней"
}, {
	id: 3,
	title: "Аллергены",
	description: "Список аллергенов"
}, {
	id: 4,
	title: "Лечения",
	description: "Полная история орбращений в медецинские учреждения и заключения обследований"
}, {
	id: 5,
	title: "Исследования",
	description: "Полная история участия в клинических исследованиях"
}];

const shareData = {
	firstName: "Harold",
	lastName: "Pain",
	image: USER_IMAGE,
	diseases: [{
		id: 1,
		code: "2315-435",
		title: "Туберкулез",
	}, {
		id: 2,
		code: "2553-234",
		title: "Хроническая обструктивная болезнь легких",
	}, {
		id: 3,
		code: "6453-242",
		title: "Малярия",
	}, {
		id: 4,
		code: "7897-231",
		title: "Рак легких, трахеи и бронхов",
	}, {
		id: 5,
		code: "2563-224",
		title: "Стоматит",
	}],
	allergens: [{
		id: 1,
		name: "Бета-лактамные антибиотики",
	}, {
		id: 2,
		name: "Арахис",
	}, {
		id: 3,
		name: "Сульфаниламиды",
	}],

};

class AuthGate extends Component {
	constructor(props) {
		super(props);

		const params = qs.parse(window.location.search.replace("?", ""));
		this.successUrl = params.success;
		this.failureUrl = params.failure;

		this.state = {
			popupOpen: false
		};
	}

	openPopup = () => {

		
		axios.request({
			url: "https://practical-day-187807.appspot.com/api/public/1/notification/alert?userHash=4535345&title=Запрос доступа&body=Пароль для разрешения доступа: 553623",
			method: "POST"
		});

		this.setState({
			popupOpen: true
		});
	}

	acceptAccess = () => {
		const url = `${this.successUrl}?d=${encodeURIComponent(JSON.stringify(shareData))}`;
		window.location.href = url;
	}

	declineAccess = () => {
		const url = this.failureUrl;
		window.location.href = url;
	}

	render = () => {
		const userpicUrl = USER_IMAGE;
		return (
			<div className={styles.root}>
				<div className={styles.container}>
					<div className={styles.header}>
						<img className={styles.logo} src={require("logo-red.png")} />
					</div>
					<div className={styles.card}>
						<div className={styles.user}>
							<div className={styles.userPic} style={{ backgroundImage: `url(${userpicUrl})` }}></div>
							<div className={styles.userName}>{shareData.firstName} {shareData.lastName}</div>
						</div>
						<div className={styles.tint}>
							<span className={styles.tintTitle}>Telemedicine Service</span>
							получит следующую информацию:
						</div>
						<div className={styles.permissions}>
							{permissions.map(permission => (
								<label key={permission.id} className={styles.permissionRow}>
									<div className={styles.checkbox}>
										<input className={styles.permissionCheckbox} type="checkbox" />
										<span className={styles.checkboxElem} />
									</div>
									<div className={styles.permissionContent}>
										<div className={styles.permissionTitle}>{permission.title}</div>
										<div className={styles.permissionDescription}>{permission.description}</div>
									</div>
								</label>
							))}

						</div>

						<button className={styles.acceptAction} onClick={this.openPopup}>Разрешить доступ</button>
					</div>
					<a className={styles.declineAction} onClick={this.declineAccess}>Отказать</a>
				</div>
				{}
				<div className={[styles.popup, this.state.popupOpen ? styles._open : null ].join(" ")}>
					<div className={styles.popupContainer}>
						<div className={styles.popupContent}>
							<div className={styles.popupText}>Введите код отправленный на ваше мобильное устройство</div>
							<input type="input" className={styles.popupInput} />
						</div>
						<button className={styles.popupButton} onClick={this.acceptAccess}>Разрешить доступ</button>
					</div>
				</div>
			</div>
		);
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthGate);