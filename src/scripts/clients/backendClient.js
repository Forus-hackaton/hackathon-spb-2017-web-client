import axios from "axios";
import qs from "qs";
import storage from "local-storage-fallback";

import * as Config from "config";
import errorHandler from "./errorHandler";

const backendClient = {
	client: axios.create({
		baseURL: Config.apiUrl,
		responseType: "json",
		headers: {
			"Content-Type": "application/x-www-form-urlencoded"
		},
		transformRequest: data => {
			return qs.stringify(data);
		},
		paramsSerializer: function(params) {
			return qs.stringify(params);
		}
	}), 
	options: {
		interceptors: {
			request: [(store, config) => {
				let token = storage.getItem(Config.accessTokenStorageKey);

				if(token === null){
					let action = store.getSourceAction(config);
					if(action.accessToken !== null)
						token = action.accessToken;
				}

				if(token) {
					config.params = config.params || {};
					config.params[Config.accessTokenParam] = token;
				}
				
				return config;
			}],
			response: []
		},
		onError: errorHandler
	}
};
 
export default backendClient;