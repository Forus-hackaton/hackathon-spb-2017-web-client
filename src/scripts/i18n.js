import i18n from "i18next";
import XHR from "i18next-xhr-backend";
import LanguageDetector from "i18next-browser-languagedetector";
import Cache from "i18next-localstorage-cache-fallback";
import numeral from "numeral";

import { constants, languages, languageDefault } from "config";
import store from "./store";
import { setCurrentLanguage } from "common/commonActions";

let versions = {};
languages.map(lang => { versions[lang] = window.BUILD_ID; });

i18n
	.use(XHR)
	.use(Cache)
	.use(LanguageDetector)
	.init({
		fallbackLng: languageDefault,
		nsSeparator: false,
		keySeparator: false,
		whitelist: languages.map(x => x),
		load: "languageOnly",
		loadPath: "/locales/{{lng}}/{{ns}}.json",
		pluralSeparator: "_",
		debug: false, //constants.ENVIRONMENT == "development",
		cache: {
			enabled: constants.ENVIRONMENT != "development",
			prefix: "i18next_res_",
			expirationTime: 7 * 24 * 60 * 60 * 1000,
			versions: versions
		},
		interpolation: {
			formatSeparator: ",",
			format: function (value, format, lng) {
				if (format === "numeralFormat") return numeral(value).format("0,0");
				if (format === "uppercase") return value.toUpperCase();
				return value;
			}
		}
	});
i18n.on("loaded", loaded => {
	var lang = i18n.language || "en";
	languages.forEach(elem => {
		if (lang.indexOf(elem) > -1) {
			var classes = languages.map(elem2 => "_" + elem2);
			document.body.classList.remove(...classes);
			document.body.classList.add("_" + elem);
			if (store.getState().app.common.currentLanguage != elem)
				store.dispatch(setCurrentLanguage(elem));
		}
	});
});
i18n.on("languageChanged", lang => {
	languages.forEach(elem => {
		if (lang.indexOf(elem) > -1) {
			var classes = languages.map(elem2 => "_" + elem2);
			document.body.classList.remove(...classes);
			document.body.classList.add("_" + elem);
			if (store.getState().app.common.currentLanguage != elem)
				store.dispatch(setCurrentLanguage(elem));
		}
	});
});

export default i18n;