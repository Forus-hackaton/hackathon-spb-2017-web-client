module.exports = {
	fontName: "gigawatt",
	files: ["./svg/*.svg"],
	baseClass: "icon",
	classPrefix: "icon-"
};